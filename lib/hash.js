//  Hashing functionality.

const crypto = require('crypto');

/**
 * Hash a string without a salt.
 * @param {string} data 
 */
function basic(data) {
    const hash = crypto.createHash('sha256');
    hash.update(data);
    return hash.digest('hex').toString();
}

/**
 * Hash a salted string
 * @param {string} data 
 * @param {number} salt 
 */
function salted(data, salt) {
    const hash = crypto.createHash('sha256');
    var salt = salt || Math.floor(Math.random() * 1000);
    hash.update(salt + data);

    return {
        hash: hash.digest('hex').toString(),
        salt: salt
    };
}

/**
 * Validate a salted hash.
 * @param {string} hash 
 * @param {number} salt 
 * @param {string} test 
 */
function validate(hash, salt, test) {
    var test = salted(test, salt);
    return test.hash === hash;
}

module.exports = {
    basic: basic,
    salted: salted,
    validate: validate
}