/**
 * models/db.js - Handle bluzelle connectivity
 */
const fs = require('fs-extra');

const { bluzelle } = require('bluzelle');

const BLUZELLE_ID = 'auth/bluzelle.uuid';
const BLUZELLE_PEM = 'auth/bluzelle.pem';

// Check if the ID and PEM file exist.
if (!fs.existsSync(BLUZELLE_ID)) {
    console.error(`Bluzelle ID file (${BLUZELLE_ID}) doesn't exist.`);
    return 1;
}
if (!fs.existsSync(BLUZELLE_PEM)) {
    console.error(`Bluzelle PEM file (${BLUZELLE_PEM}) doesn't exist.`);
    return 1;
}

// Create the connection object
const db = bluzelle({
    entry: 'ws://testnet.bluzelle.com:51010',
    uuid: fs.readFileSync(BLUZELLE_ID).toString(),
    private_pem: fs.readFileSync(BLUZELLE_PEM).toString()
});

async function initializeDatabase() {
    try {
        console.log('Initializing Database.');
        //db.deleteDB();
        const has = await db.hasDB();
        if (!has) {
            await db.createDB();
            await db.create('users', JSON.stringify({}));
            await db.create('posts', JSON.stringify({}));
            console.log('Database created');
        }
        console.log('Database initialized');
    } catch (e) {
        throw e;
    }
}

initializeDatabase();
/**
 * Reads the specified key from the database and parses it with JSON.
 * @param {string} key The value to read.
 * @returns {object} The value of the key (parsed in JSON format) or an empty object if it doesn't exist.
 */
async function read(key) {
    if (await db.has(key)) {
        return JSON.parse(await db.read(key));
    }
    return {};
}
/**
 * Writes an object to the database. Creates the key if it doesn't exist.
 * @param {string} key The name of the key to write to the database.
 * @param {object} value The value of the object, will be stringified and written to the database.
 */
async function write(key, value) {
    let data = JSON.stringify(value);
    if (await db.has(key)) {
        await db.update(key, data);
    } else {
        await db.create(key, data);
    }
}

module.exports = {
    db: db,
    read: read,
    write: write
};