const express = require('express')
    , bodyParser = require('body-parser')
    , session = require('express-session')
    , passport = require('passport')
    , flash = require('connect-flash')
    , nunjucks = require('nunjucks');

let db = require('./models/db');

const app = express();
const PORT = 80;

app.use(express.static('src'));

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
    extended: true
}));

app.use(session({
    resave: false,
    saveUninitialized: true,
    secret: 'cats'
}));
app.use(flash());

app.use(passport.initialize());
app.use(passport.session());

nunjucks.configure('views', {
    noCache: true,
    autoescape: true,
    express: app
});

app.use('/login', require('./controllers/login'));
app.use('/posts', require('./controllers/posts'));
app.use('/users', require('./controllers/users'));
app.use('/profile', require('./controllers/profile'));


app.get('/', (request, response) => {
    if (request.isAuthenticated()) {
        response.render('index.html', {
            authenticated: true,
            user: request.user.id
        });
    } else {
        response.redirect('/login');
    }
});

app.listen(PORT, () => {
    console.log(`Listening on port ${PORT}`);
});