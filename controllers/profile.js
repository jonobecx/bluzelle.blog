const express = require('express')
    , router = express.Router();

const db = require('../models/db');
const hash = require('../lib/hash')
/**
 * GET /profile
 * Show the user's profile if they are authenticated
 */
router.get('/', async (request, response, next) => {
    try {
        if (request.isAuthenticated()) {
            let key = 'profile/' + hash.basic(request.user.id);
            let profile = await db.read(key);
            response.render('profile.html', {
                flash: request.flash(),
                profile: profile,
                authenticated: request.isAuthenticated()
            });
        } else {
            response.redirect('/login?redirect=profile')
        }
    } catch (e) {
        next(e);
    }
});

/**
 * PUT /profile
 * Update the user's profile
 */
router.put('/', async (request, response, next) => {
    try {
        if (request.isAuthenticated()) {
            let user_hash = hash.basic(request.user.id);
            let key = 'profile/' + user_hash;

            // TODO: Validate properties
            let profile = request.body;

            let handles = await db.read('handles');
            handles[user_hash] = profile.handle;            
            await db.write('handles', handles);

            await db.write(key, profile);

            request.flash('success', 'Profile Updated!');
            response.redirect('/profile');
        } else {
            response.redirect('/login?redirect=profile');
        }
    } catch (e) {
        next(e);
    }
});

module.exports = router;