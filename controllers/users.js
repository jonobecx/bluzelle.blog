const express = require('express')
    , router = express.Router();

const db = require('../models/db.js');

/**
 * GET /users
 * Get a list of users in json format
 */
router.get('/',
    async (request, response, next) => {
        try {
            if (!request.isAuthenticated()) {
                return response.redirect('/login');
            }
            const users = await db.read('users');
            response.send(users);
        } catch (e) {
            next(e);
        }
    }
);

/**
 * POST /users
 * Create a new user.
 */
router.post('/',
    async (request, response, next) => {
        try {
            const users = await db.read('users');
            const key = hashUser(request.body.email);

            if (users.hasOwnProperty(key)) {
                throw new Error('User Exists!');
            } else {
                users[key] = {
                    email: request.body.email,
                    password: hashPassword(request.body.password)
                }
            }
            // Create profile
            await db.write('profile/' + key, {});
            // Update user index
            await db.write('users', users);

            response.redirect('/login');
        } catch (e) {
            next(e);
        }
    }
);

module.exports = router;
