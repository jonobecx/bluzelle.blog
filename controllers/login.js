// Login functionality; implements passport's sample login functionality using the local authentication strategy.

const express = require('express')
    , flash = require('connect-flash')
    , passport = require('passport')
    , LocalStrategy = require('passport-local').Strategy
    , router = express.Router();

const db = require('../models/db.js');
const hash = require('../lib/hash');

// Initialize the local authentication strategy.
passport.use(new LocalStrategy((username, password, done) => {
    // Use the database directly in this case because initializng a new object with an async function doesn't seem to work.
    db.db.read('users')
        .then(data => {
            let users = JSON.parse(data);
            var user = hash.basic(username);

            if (users[user]) {
                let auth = users[user].password;
                if (!hash.validate(auth.hash, auth.salt, password)) {
                    return done(null, false, { message: 'Incorrect password.' });
                }
            } else {
                return done(null, false, { message: 'Incorrect username.' });
            }
            return done(null, user);
        })
        .catch(message => {
            throw new Error(message);
        });
}));

// Implement the passport user serialization function.
passport.serializeUser(function (user, done) {
    done(null, user);
});

// Implement the passport user deserialization function.
passport.deserializeUser(function (id, done) {
    var error = null;
    var user = null;
    // Use the database directly here too because async functions don't work.
    db.db.read('users').then(data => {
        let raw_user = JSON.parse(data)[id];
        user = {
            id: raw_user.email
        };
        raw_user = null;
        done(error, user);
    }).catch(error => {
        done(error, user);
    })
});

/**
 * GET /login
 * Get the login page.
 */
router.get('/', (request, response, next) => {
    response.render('login.html', {
        flash: request.flash(),
        redirect: request.query.redirect
    });
});

/**
 * POST /login
 * Logs the user in.
 */
router.post('/', (request, response, next) => {
    passport.authenticate('local', function (err, user, info) {
        if (err) { return next(err); }
        if (!user) { return response.redirect('/login'); }
        request.logIn(user, function (err) {
            if (err) { return next(err); }
            return response.redirect('/' + request.body.redirect);
        });
    })(request, response, next);
});


module.exports = router;