const express = require('express')
    , router = express.Router();

const db = require('../models/db.js');
const hash = require('../lib/hash.js');

/**
 * GET /posts
 * Get a list of all posts in the database.
 */
router.get('/',
    async (request, response, next) => {
        try {
            var raw_posts = await db.read('posts');
            var handles = await db.read('handles');

            let posts = [];
            for (let k in raw_posts) {
                post = raw_posts[k];
                post.id = k;
                // Figure out the author name by cross referencing with the handles collection.
                post.author = handles[post.author]
                posts.push(post);
            }
            response.render('posts.html', {
                posts: posts,
                authenticated: request.isAuthenticated()
            });
        } catch (e) {
            next(e);
        }
    }
);

/**
 * GET /posts/postId
 * Get a single post and display it.
 */
router.get('/:postId',
    async (request, response, next) => {
        try {
            var raw_posts = await db.read('posts');
            var handles = await db.read('handles');

            let post = raw_posts[request.params.postId];
            post.author = handles[post.author];

            response.render('post.html', {
                post: post,
                authenticated: request.isAuthenticated()
            });
        } catch (e) {
            next(e);
        }
    });

/**
 * POST /posts
 * Crete a new post.
 */
router.post('/',
    async (request, response, next) => {
        try {
            if (!request.isAuthenticated()) {
                return response.redirect('/login');
            }
            let posts = await db.read('posts');
            if (!(request.body.subject || request.body.content)) {
                throw new Error("Malformed request.");
            }
            let post = {
                author: hash.basic(request.user.id),
                timestamp: new Date().toISOString(),
                subject: request.body.subject,
                content: request.body.content,
                likes: 0
            };
            // Generate a key for the post by hashing the object.
            posts[hash.basic(JSON.stringify(post))] = post;
            await db.write('posts', posts);
            return response.redirect('/posts');
        } catch (e) {
            next(e);
        }
    }
);

module.exports = router;