$('#submit_button').on('click', event => {
    if ($('#password').val() !== $('#password_confirm').val()) {
        console.error('Passwords must match');
        event.preventDefault();
    }
});