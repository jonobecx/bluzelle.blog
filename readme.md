# Bluzelle Blog

## Overview

A simple blog using Bluzelle as a database. Currently has the following endpoints:

 - GET `/` Index of Interfaces
 - GET `/signup` Interface to register a new user in the system.
 - POST `/users` Creates a new user in the system.
 - GET `/users` JSON list of users in the system.
 - GET `/login` Interface to login to the system.
 - POST `/login` Logs in as an existing user and starts a cookie based session.
 - GET `/profile` Interface for the profile settings.
 - POST `/profile` Update profile settings.
 - GET `/posts` Interface to a list of posts.
 - POST `/posts` Create a new post.

## Installation

1. Create `bluzelle.uuid`, a text file containing the uuid of the bluzelle database to connect to.
2. Create `bluzelle.pem`, a text file containing the secret key for the bluzelle database to connect to.
3. `npm install`
4. `npm start`
5. Application should be running at `http://localhost:80`

## Technology Used

* [Bluzelle.js](https://docs.bluzelle.com/bluzelle-js/)
* [Express.js](https://expressjs.com/)
* [Passport.js](https://www.passportjs.org/)
* [Nunjucks](https://mozilla.github.io/nunjucks/)
* [Bootstrap](https://getbootstrap.com/)
* [SimpleMDE](https://simplemde.com/)
* [marked.js](https://marked.js.org/)

# WARNING

This is not an optimized application and will probably crash if there are lots of blog posts or in many other situations. It hasn't been tested for security or performance and shouldn't be used for anything other than experimentation.
